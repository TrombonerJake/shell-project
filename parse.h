//
// Created by jkarpovich on 2/16/22.
//

#ifndef SHELLPROJECT_PARSE_H
#define SHELLPROJECT_PARSE_H

#define MAX_LENGTH 255

// Function declaration
void lineToWords(char buffer[]);
void doCommand(int arg_count, char* command[]);

#endif //SHELLPROJECT_PARSE_H