//
// Created by jkarpovich on 3/10/22.
//

#include <string.h>
#include <unistd.h>
#include "error.h"

extern char *path[];

int check_path(char *command[], char *new_path) {
    int i = 0;

    while (path[i] != NULL) {
        strcpy(new_path, path[i]);
        strcat(new_path, "/");
        strcat(new_path, command[0]);

        if (access(new_path, X_OK) == 0) {
            return 1;
        }

        i++;
    }

    return 0;
}