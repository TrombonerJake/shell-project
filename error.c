//
// Created by jkarpovich on 2/21/22.
//
#include <unistd.h>
#include "string.h"
#include <stdlib.h>

void throw_error() {
    char error_message[30] = "An error has occurred\n";
    write(STDERR_FILENO, error_message, strlen(error_message));
}
