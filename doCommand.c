//
// Created by jfkar on 2/20/2022.
//

#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include "error.h"
#include "create_path.h"

#include <stdio.h>

extern char *path[];

void doCommand(int arg_count, char *command[]) {
    // BUILT-INS: exit, cd, path
    if (strcmp(command[0], "exit") == 0) {
        if (arg_count > 1) {
            throw_error();
        }
        exit(EXIT_SUCCESS);
    } else if (strcmp(command[0], "cd") == 0) {
        if (arg_count == 2) {
            chdir(command[1]);
        } else {
            throw_error();
        }
    } else if (strcmp(command[0], "path") == 0) {
        path[0] = NULL;
        int i = 0;

        while (i < arg_count - 1) {
            path[i] = strdup(command[i + 1]);
            i++;
        }

        path[i + 1] = NULL;

    } else {
        if (path[0] != NULL) {
            char new_path[255];

            if (check_path(command, new_path)) {
                pid_t pid = fork();

                if (pid > 0) {
                    wait(NULL);
                } else if (pid == 0) {
                    execv(new_path, command);
                } else {
                    throw_error();
                }
            } else {
                throw_error();
            }
        } else {
            throw_error();
        }
    }
}
