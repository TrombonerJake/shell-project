//
// Created by jkarpovich on 2/20/22.
//

#include "parse.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "error.h"
#include <unistd.h>
#include <fcntl.h>

#include <stdio.h>

// The Trim Adventures of Billy & Mandy
char *trimString(char *str) {
    char *end;

    while (isspace((unsigned char) *str)) str++;

    if (*str == 0)
        return str;

    end = str + strlen(str) - 1;
    while (end > str && isspace((unsigned char) *end)) end--;

    if (*end == '\n') {
        end--;
    }

    end[1] = '\0';

    return str;
}

void lineToWords(char buffer[]) {
    char tempBuffer[255];
    char source[255];
    char output[255];
    int count = 0;
    int arg_count = 0;
    int i = 0;
    char *command[MAX_LENGTH];

    while (buffer[i] != '\0') {
        if (buffer[i] == ' ') {
            arg_count++;
        }
        i++;
    }
    arg_count++;

    strcpy(tempBuffer,buffer);                                      //copies buffer to tempBuffer to avoid modifying buffer
    strcpy(source, tempBuffer);
    trimString(source);
    trimString(tempBuffer);

    if (strstr(tempBuffer, ">") != NULL) {
        //TEST 10: Multiple >
        int count = 0;
        const char *tmp = tempBuffer;
        while ((tmp = strstr(tmp, ">"))) {
            count++;
            tmp++;
        }
        if (count > 1) {
            throw_error();
            exit(EXIT_SUCCESS);
        }

        strcpy(source, trimString(strtok(tempBuffer, ">")));

        // THIS WORKS FOR 8, 9, and 12 (Redirect no command) ... Make Seg Faults Illegal Again
        char *tempOutput = strtok(NULL, ">");
        if (tempOutput == NULL) {
            throw_error();
            exit(EXIT_SUCCESS);
        }

        strcpy(output, trimString(tempOutput));

        //TEST 8: Redirection with no output file specified CODENAME: MOZZARELLA
        if (output[0] == '\0') {
            throw_error();
            exit(EXIT_SUCCESS);
        }

        //TEST 9: Multiple output files CODENAME: COLBY JACK
        if (strstr(output, " ") != NULL) {
            throw_error();
            exit(EXIT_SUCCESS);
        }

        FILE *fd = fopen(output, "w");
        dup2(STDOUT_FILENO, fileno(fd));
        dup2(STDERR_FILENO, fileno(fd));

        //int fd = open(output, O_WRONLY, 0666);
        //dup2(STDOUT_FILENO, fd);
        //dup2(STDERR_FILENO, fd);

        //dup2(fd, STDOUT_FILENO);
        //dup2(fd, STDERR_FILENO);
        //close(fd);
    }

    char *words = strtok(source, " \n");                         //strtok remove new line
    while (words != NULL) {                                            //loop for remaining words
        command[count] = words;

        words = strtok(NULL, " \n");
        count++;
    }

    // Supply and Command
    command[count] = NULL;
    doCommand(arg_count, command);
}