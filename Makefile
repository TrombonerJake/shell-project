all: wish

wish: main.o parse.o doCommand.o create_path.o error.o
	gcc -o wish main.o parse.o doCommand.o create_path.o error.o

main.o: main.c
	gcc -c main.c

parse.o: parse.c parse.h
	gcc -c parse.c

doCommand.o: doCommand.c
	gcc -c doCommand.c

create_path.o: create_path.c create_path.h
	gcc -c create_path.c

error.o: error.c error.h
	gcc -c error.c

clean:
	rm main.o parse.o doCommand.o create_path.o error.o wish