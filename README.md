# Shell Project

Shell Project is a simple shell created for an assignment in CMPE 320 Operating Systems.

## Installation

Don't.

## Usage

```c stdlibhighlighting
# returns 'wish> ' and waits for user input
./wish

# returns 'line# <line_text>' and exits after EOF
./wish [filename.txt]
```
