#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parse.h"
#include "error.h"
#include <stdbool.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/wait.h>

char *path[255] = {"/bin", NULL};
int main(int argc, char *argv[]) {
    //sets up variables to be used
    char buffer[MAX_LENGTH];
    int i = 0;

    //INTERACTIVE
    if (argc == 1) {                                                 //1 arg, enter wish
        while (true) {                                               //loop to accept input
            i = 0;
            printf("wish> ");                                        //prompt
            fgets(buffer, MAX_LENGTH, stdin);                        //take input
            while(isspace(buffer[i])) {
                i++;
            }
            if (strcmp(&buffer[i],"\0") != 0) {
                if (strstr(buffer, "&") != NULL) {
                    char *command = strtok(buffer, "&");

                    pid_t pids[255];
                    int count = 0;
                    while (command != NULL && *command != '\0') {
                        pid_t pid = fork();

                        if (pid > 0) {
                            pids[count] = pid;
                            count++;

                        } else if (pid == 0) {
                            lineToWords(command);
                            exit(EXIT_SUCCESS);
                        }

                        command = strtok(NULL, "&");
                    }

                    for(int i = 0; i < count; i++){
                        waitpid(pids[i], NULL, 0);
                    }
                } else {
                    lineToWords(buffer);
                }
            }
        }
    }

        //BATCH
    else if (argc == 2) {                                            //2 arg, file input
        char *filename = argv[1];                                    //gets filename from argv[1]
        FILE *fp = fopen(filename, "r");                             //opens file

        if (fp == NULL) {                                            //checks for empty file
            throw_error();
            exit(1);
        }

        while (fgets(buffer, MAX_LENGTH, fp)) {                      //prints line by line
            i = 0;
            while (isspace(buffer[i])) {
                i++;
            }
            if (strcmp(&buffer[i],"\0") != 0) {
                if (strstr(buffer, "&") != NULL) {
                    char *command = strtok(buffer, "&");

                    pid_t pids[255];
                    int count = 0;
                    while (command != NULL && *command != '\0') {
                        pid_t pid = fork();

                        if (pid > 0) {
                            pids[count] = pid;
                            count++;

                        } else if (pid == 0) {
                            lineToWords(command);
                            exit(EXIT_SUCCESS);
                        }

                        command = strtok(NULL, "&");
                    }

                    for(int i = 0; i < count; i++){
                        waitpid(pids[i], NULL, 0);
                    }
                } else {
                    lineToWords(buffer);
                }
            }
        }

        fclose(fp);                                                  //close file

        exit(EXIT_SUCCESS);

    } else
        throw_error();
    exit(1);                                          //exit with error
}
